# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Partner title order",
    "summary": "Makes partner title sortable by sequence",
    "version": "2.0.1.0.0",
    "category": "Hidden",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "base",
    ],
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "data": [
        "views/res_partner_title_views.xml",
    ],
    "installable": True,
}
