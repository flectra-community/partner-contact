{
    "name": "Partner Identification Numbers Unique By Category",
    "category": "Customer Relationship Management",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "depends": ["partner_identification"],
    "data": ["views/res_partner_id_category_view.xml"],
    "author": "Camptocamp SA, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "development_status": "Production/Stable",
}
