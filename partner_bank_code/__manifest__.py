# Copyright 2021 Ecosoft Co., Ltd. (https://ecosoft.co.th)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Partner Bank Code",
    "summary": "Add fields information in banks",
    "version": "2.0.1.1.0",
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": ["views/res_bank.xml"],
}
