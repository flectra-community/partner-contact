# Copyright 2017 initOS GmbH
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Translate Country States",
    "version": "2.0.1.0.0",
    "depends": ["base"],
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "summary": "Translate Country States",
    "author": "initOS GmbH, Odoo Community Association (OCA)",
    "category": "Localisation",
    "license": "AGPL-3",
    "installable": True,
}
