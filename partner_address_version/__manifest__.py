# Copyright 2018 Akretion - Benoît Guillot
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Partner Address Version",
    "version": "2.0.1.0.0",
    "author": "Akretion, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "category": "CRM",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        "base",
    ],
}
