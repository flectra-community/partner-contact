# Copyright 2018 Tecnativa - David Vidal
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Portal Partner Select All",
    "version": "2.0.1.1.0",
    "category": "Custom",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/partner-contact",
    "license": "AGPL-3",
    "depends": ["portal"],
    "data": ["wizard/portal_wizard.xml"],
    "installable": True,
}
