# Flectra Community / partner-contact

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[partner_tier_validation](partner_tier_validation/) | 2.0.3.0.2| Support a tier validation process for Contacts
[partner_disable_gravatar](partner_disable_gravatar/) | 2.0.1.0.0| Disable automatic connection to gravatar.com
[partner_company_type](partner_company_type/) | 2.0.1.0.1| Adds a company type to partner that are companies
[partner_deduplicate_filter](partner_deduplicate_filter/) | 2.0.1.0.0| Exclude records from the deduplication
[partner_contact_birthplace](partner_contact_birthplace/) | 2.0.1.0.0| This module allows to define a birthplace for partners.
[partner_identification_gln](partner_identification_gln/) | 2.0.1.0.1|         This addon extends "Partner Identification Numbers"        to provide a number category for GLN registration
[partner_ref_unique](partner_ref_unique/) | 2.0.1.0.0| Add an unique constraint to partner ref field
[partner_employee_quantity](partner_employee_quantity/) | 2.0.1.0.0| Know how many employees a partner has
[partner_company_group](partner_company_group/) | 2.0.1.1.0| Adds the possibility to add a company group to a company
[partner_contact_age_range](partner_contact_age_range/) | 2.0.1.0.1| Age Range for Contact's
[partner_label](partner_label/) | 2.0.1.0.1| Print partner labels
[partner_identification_notification](partner_identification_notification/) | 2.0.1.0.0| Partner Identification Notification
[partner_capital](partner_capital/) | 2.0.1.0.0| Partners Capital
[base_location](base_location/) | 2.0.1.2.4| Enhanced zip/npa management system
[base_location_nuts](base_location_nuts/) | 2.0.1.0.1| NUTS Regions
[partner_contact_access_link](partner_contact_access_link/) | 2.0.1.0.0| Allow to visit the full contact form from a company
[partner_contact_department](partner_contact_department/) | 2.0.1.0.1| Assign contacts to departments
[partner_priority](partner_priority/) | 2.0.1.0.0| Adds priority to partners.
[partner_phone_secondary](partner_phone_secondary/) | 2.0.1.0.0| Adds a secondary phone number on partners
[partner_contact_in_several_companies](partner_contact_in_several_companies/) | 2.0.1.1.1| Allow to have one contact in several partners
[partner_second_lastname](partner_second_lastname/) | 2.0.1.1.0| Have split first and second lastnames
[partner_contact_gender](partner_contact_gender/) | 2.0.1.0.1| Add gender field to contacts
[partner_stage](partner_stage/) | 2.0.2.1.1| Add lifecycle Stages to Partners
[partner_external_map](partner_external_map/) | 2.0.1.0.0| Add Map and Map Routing buttons on partner form to open GMaps, OSM, Bing and others
[partner_deduplicate_by_ref](partner_deduplicate_by_ref/) | 2.0.1.0.0| Deduplicate Contacts by reference
[partner_deduplicate_acl](partner_deduplicate_acl/) | 2.0.1.0.0| Contact deduplication with fine-grained permission control
[partner_tz](partner_tz/) | 2.0.1.0.0| Remove partner timezone default value and display on form
[partner_contact_lang](partner_contact_lang/) | 2.0.1.0.0| Manage language in contacts
[partner_vat_unique](partner_vat_unique/) | 2.0.1.0.0| Module to make the VAT number unique for customers and suppliers.
[partner_manual_rank](partner_manual_rank/) | 2.0.1.0.0| Be able to manually flag partners as customer or supplier.
[partner_contact_job_position](partner_contact_job_position/) | 2.0.1.0.0| Categorize job positions for contacts
[partner_firstname](partner_firstname/) | 2.0.1.1.0| Split first name and last name for non company partners
[partner_contact_personal_information_page](partner_contact_personal_information_page/) | 2.0.1.0.0| Add a page to contacts form to put personal information
[partner_contact_birthdate](partner_contact_birthdate/) | 2.0.1.0.0| Contact's birthdate
[portal_partner_select_all](portal_partner_select_all/) | 2.0.1.1.0| Portal Partner Select All
[partner_pricelist_search](partner_pricelist_search/) | 2.0.1.0.1| Partner pricelist search
[partner_bank_code](partner_bank_code/) | 2.0.1.1.0| Add fields information in banks
[partner_address_street3](partner_address_street3/) | 2.0.1.0.0| Add a third address line on partners
[partner_deduplicate_by_website](partner_deduplicate_by_website/) | 2.0.1.0.0| Deduplicate Contacts by Website
[animal](animal/) | 2.0.1.1.0| Manage animals information
[partner_coc](partner_coc/) | 2.0.1.0.1| Adds field 'Chamber Of Commerce Registration Number'
[partner_multi_relation](partner_multi_relation/) | 2.0.1.1.0| Partner Relations
[partner_fax](partner_fax/) | 2.0.1.0.0| Add fax number on partner
[animal_owner](animal_owner/) | 2.0.1.2.0| Add owner to the animal
[base_partner_sequence](base_partner_sequence/) | 2.0.1.0.1| Sets customer's code from a sequence
[partner_iterative_archive](partner_iterative_archive/) | 2.0.1.0.1| Archive all contacts when parent is archived
[base_country_state_translatable](base_country_state_translatable/) | 2.0.1.0.0| Translate Country States
[partner_affiliate](partner_affiliate/) | 2.0.1.0.1| Partner Affiliates
[partner_phone_extension](partner_phone_extension/) | 2.0.1.0.1| Partner Phone Number Extension
[partner_industry_secondary](partner_industry_secondary/) | 2.0.1.0.0| Add secondary partner industries
[partner_title_order](partner_title_order/) | 2.0.1.0.0| Makes partner title sortable by sequence
[partner_contact_nationality](partner_contact_nationality/) | 2.0.1.0.1| Add nationality field to contacts
[partner_email_check](partner_email_check/) | 2.0.1.1.1| Validate email address field
[partner_identification_unique_by_category](partner_identification_unique_by_category/) | 2.0.1.0.1| Partner Identification Numbers Unique By Category
[base_location_geonames_import](base_location_geonames_import/) | 2.0.1.0.2| Import zip entries from Geonames
[partner_phonecall_schedule](partner_phonecall_schedule/) | 2.0.1.0.0| Track the time and days your partners expect phone calls
[partner_contact_address_default](partner_contact_address_default/) | 2.0.1.1.0| Set a default delivery and invoice address for contacts
[partner_address_version](partner_address_version/) | 2.0.1.0.0| Partner Address Version
[partner_identification](partner_identification/) | 2.0.1.3.0| Partner Identification Numbers
[partner_helper](partner_helper/) | 2.0.1.0.0| Add specific helper methods


